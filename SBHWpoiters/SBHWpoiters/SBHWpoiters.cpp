﻿

#include <iostream>
#include <string>

class Player {
private:
    std::string playerName;
    int playerPoints{};
public:
    Player()
    {}
    Player(std::string name , int points)
    {
        playerName = name;
        playerPoints = points;
    }
    int getPoints() {
        return playerPoints;
    }
    std::string getName() {
        return playerName;
    }

};
void fillArray(Player* arr, int size) {

    std::string tempN;
    int tempP;

    for (int i = 0; i < size; i++) {

        std::cout << "Enter player " << i + 1 << " name: ";
        std::cin >> tempN;
        std::cout << std::endl;

        std::cout << "Enter player " << i + 1 << " points: ";
        std::cin >> tempP;
        std::cout << std::endl;

        arr[i] = Player(tempN, tempP);
    }
}
void bubbleSort(Player* arr, int size) {
    for (int i = 0; i < size - 1; i++)
    {
        for (int j = 0; j < size - i - 1; j++) {

            if (arr[j].getPoints() < arr[j + 1].getPoints()) {

                Player temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }

        }
    }
}


    int main()
    {
        
        int playersAmount{};

        std::cout << "Enter number of players: ";
        std::cin >> playersAmount;
        std::cout << std::endl;

        Player* arr = new Player[playersAmount];
        fillArray(arr, playersAmount);
        bubbleSort(arr, playersAmount);
        for (int i = 0; i < playersAmount; i++) {
            std::cout << arr[i].getName() << std::endl;
        }
        delete[] arr;
       

    }
